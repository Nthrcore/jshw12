document.addEventListener('keydown', (event)=>{
    const btn = document.querySelectorAll('.btn');
    btn.forEach((item)=>{
        item.style.background = '';
        const btnText = item.textContent.toLowerCase();
        if (event.key === btnText){
            item.style.background = 'blue';
        }
    })
})
